'use strict'
const axios = require('axios')
const {WebhookClient} = require('dialogflow-fulfillment')
process.env.DEBUG = 'dialogflow:debug'

module.exports = (request, response) => {

    const agent = new WebhookClient({ request, response })
    
    //console.log("wwwwwwwwwwwwwwwwwwwwwwwwwww  agent.request_.body")
    
    console.log("===== PARAMETER : ")
    console.log(agent.request_.body.originalDetectIntentRequest)
    //console.log(agent.originalRequest.payload)
    console.log(".----------.")

    function welcome(agent) {
      agent.add(`Texto forzado :v`)
    }
   
    function fallback(agent) {
      agent.add(`I didn't understand`)
      agent.add(`----I'm sorry, can you try again?`)
    }

    async function load_promocion(){
      let result = await getPromocionDisponible()
      if(result.success){
        return `Llegó la promocion "${result.promociones[0].nombre}". Para participar necesitas ${result.promociones[0].legales}`
      }else{
        return "Ahorita no contamos con promociones disponibles =("
      }
    }

    
    const extractEmails = (email) => {
      return email.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)/gi);
    }

    const capturar_nombres = async ( agent ) => {
      const all_name_message = request.body.queryResult.queryText
      agent.context.delete('input_nombres')
      agent.context.set({name:'input_email',lifespan:2})
      agent.context.set({name:'start_oferta',lifespan:2,parameters:{full_name:all_name_message}})

      agent.add("Por último, necesitamos tu correo electrónico")
      console.log("=>capturar_nombres--")
      console.log(agent.context.get('start_oferta'))
      console.log(agent.context.get)
      console.log("<=")
    }


    const capturar_imagenes = async (agent) => {
      const parameters = agent.parameters
      console.log("***************** PARAMETROS PRE DETECTADOS *****************")
      console.log(agent.context.get('input_imagenes'))
      const param_preDetect = agent.context.get('input_imagenes');
      // console.log(typeof(param_preDetect.parameters.numBoleta)+"length: "+param_preDetect.parameters.numBoleta.length)
      // console.log(typeof(param_preDetect.parameters.ruc)+"length: "+param_preDetect.parameters.ruc.length)
      // console.log(param_preDetect.parameters.fecha)
      // console.log(param_preDetect.parameters.numBoleta)
      // console.log(param_preDetect.parameters.montTotal)
      console.log("***************** parametros detectados guardados anteriormente en una CAPTURA IMAGEN *********************")
      console.log(parameters)
      console.log("**************************************")
      const texto = request.body.queryResult.queryText
      let since = texto.length - 11
      const telefono = texto.substring(since)
      const url_imagen = parameters.url;

      const to_send = {
        documento_identidad : parameters.dni,
        nombres : parameters.full_name,
        correo : parameters.email,
        telefono: telefono
      }
/* 
      console.log("**************+ to_send **************+")
      console.log(to_send)
      console.log("********* end ---- to_send ***********+") */

/*       console.log("---->>> antes de load_ocr")
      let res_ocr = await load_ocr(url_imagen)
      console.log("===>>> res_ocr url de imagen:")
      console.log(res_ocr) */

      console.log("===> Parametros Capturado <===")
      console.log(agent.originalRequest.payload)
/*       agent.originalRequest.payload.forEach(element => {
        console.log(`\t${element}`+"typeof => "+typeof(element))
      });
 */      console.log("===> ____________________ <===")
      
      /* 
      console.log("------->>>> datos norm <<<<--------")
      let numBoleta = (res_ocr.NumBOLETA.length>0)?(res_ocr.numBoleta[0]):("")
      console.log("numBoleta"+numBoleta)
      let fechaBoleta = (res_ocr.FECHA.length>0)?(res_ocr.FECHA[0]):("")
      console.log("fechaBoleta"+fechaBoleta)
      let rucBoleta = (res_ocr.RUC.length>0)?(res_ocr.RUC[0]):("")
      console.log("rucBoleta"+rucBoleta)
      let montTotalBoleta = (res_ocr.MontTOTAL.length>0)?(res_ocr.MontTOTAL[0]):("")
      console.log("montTotalBoleta"+montTotalBoleta)
 */

      let time_context = 0

      time_context= (agent.originalRequest.payload.numBoleta==="")?(2):(0)
      console.log("agent.originalRequest.payload.numBoleta = >>>>>")
      console.log(agent.originalRequest.payload.numBoleta)
      console.log("time_context:",time_context)
      console.log("<<<<<<")

      
      const params = {
        'fecha' : [agent.originalRequest.payload.fecha].filter(Boolean),
        'numBoleta' : [agent.originalRequest.payload.numBoleta].filter(Boolean),
        'ruc' : [agent.originalRequest.payload.ruc].filter(Boolean),
        'montTotal' : [agent.originalRequest.payload.montTotal].filter(Boolean)
      }
      console.log("====> params <====")
      console.log(params)
      console.log("==> End params <==")


      /**
       * Revisando los parametros capturados anteriormente
       */
      // fecha
      if (params.fecha.filter(e => !param_preDetect.parameters.fecha.includes(e)).length>0){
        console.log("===========>   Add new element: fecha")
        param_preDetect.parameters.fecha = param_preDetect.parameters.fecha.concat(params.fecha.filter(e => !param_preDetect.parameters.fecha.includes(e)))
      }
      if (params.numBoleta.filter(e => !param_preDetect.parameters.numBoleta.includes(e)).length>0){
        console.log("===========>   Add new element: numBoleta")
        param_preDetect.parameters.numBoleta = param_preDetect.parameters.numBoleta.concat(params.numBoleta)
      }
      if (params.ruc.filter(e => !param_preDetect.parameters.ruc.includes(e)).length>0){
        console.log("===========>   Add new element: ruc")
        param_preDetect.parameters.ruc = param_preDetect.parameters.ruc.concat(params.ruc)
      }
      if (params.montTotal.filter(e => !param_preDetect.parameters.montTotal.includes(e)).length>0){
        console.log("===========>   Add new element: montTotal")
        param_preDetect.parameters.montTotal = param_preDetect.parameters.montTotal.concat(params.montTotal)
      }

      console.log("************* param_preDetect con los nuevos elementos")
      console.log(param_preDetect)
      console.log("************* ****************************************")


      console.log("<<<=== fin de url --- res_ocr")
      
      agent.context.set(
        { name:'input_imagenes',
          lifespan:time_context,
          parameters:{
            numBoleta:  param_preDetect.parameters.numBoleta,
            ruc:        param_preDetect.parameters.ruc,
            fecha:      param_preDetect.parameters.fecha,
            montTotal:  param_preDetect.parameters.montTotal
          }
        });


      axios.post('http://121analytics.com:2110/save_person',to_send)
      

      console.log("typeof (agent.originalRequest.payload.numBoleta) = " + typeof(agent.originalRequest.payload.numBoleta)+ "=>"+agent.originalRequest.payload.numBoleta)
      
      if(agent.originalRequest.payload.numBoleta==""){
        console.log("==========>>>>> numBoleta vacio <<<<<<==========")
        //agent.add("Porfavor, mande una foto donde se aprecie mejor el numero de boleta :D")
        agent.add("¡Uy! Por favor mándame una foto donde se aprecie mejor el número de la boleta")
        
      }else{
        console.log("==========>>>>> numBoleta NO vacio <<<<<<==========")
        agent.add("¡Muchas gracias! Pronto te confirmaré tu participación. ")
        

        
        //agent.add("¡Gracias!, en menos de 24hrs se contactaran contigo para cualquier verficación :) ")
      }
      console.log("***************************************** ENTITY CAPTURADO *****************************************")
      console.log("=> start_oferta <=")
      console.log(agent.context.get('start_oferta'))
      console.log("<=")
      console.log("=> input_imagenes <=")
      console.log(agent.context.get('input_imagenes'))
      console.log("<=")
      console.log("***************************************** ---------------- *****************************************")
    }

    const capturar_email = async (agent) => {
      const text = agent.query
      let val_email = extractEmails(text)

      if(val_email){
        let promocion = await load_promocion()
        console.log("========================== RETURN PROMOCION LOAD ==========================")
        console.log(promocion)
        console.log("fin")

        if(promocion == "Ahorita no contamos con promociones disponibles =("){
          agent.context.set({name:'input_despedida',lifespan:0})
          agent.add("¡Gracias!, pronto te contaremos de nuevas promociones :)");
        
          agent.context.delete('input_email')
         ///////////////////////============
          //agent.add("Ahora ya puedes enviarnos la(s) imagene(s) de los voucher(s) para participar en la promoción : )")
          
          
        /** NEXT STEP */
        ///if(promocion != "Ahorita no contamos con promociones disponibles =("){
          //agent.context.set({name:'input_imagenes',lifespan:0})
          //agent.add("¡Gracias!, pronto te contaremos de nuevas promociones :)");
        }else{
          agent.context.set({name:'input_imagenes',lifespan:2})
          agent.context.delete('input_email')
          ///////////////////////============
          agent.add("¡Listo! Ahora ya puedes enviarme la(s) imágen(es) de las boletas para participar en la promoción.")
        }
        ////////////ç
        agent.context.set(
          { name:'start_oferta',
            lifespan:2,
            parameters:{
              email:val_email[0]
            }
          })
        
      }else{
        agent.add("No se encontró un correo")
      } 
      console.log("=>capturar email")
      console.log(agent.context.get('start_oferta'))
      console.log("<=")
    }

    async function mostrar_promocion(agent){
      /** SHOWING PROMO DATA */
      let promocion = await load_promocion()
      agent.add(promocion)
      
      /** NEXT STEP */
      if(promocion != "Ahorita no contamos con promociones disponibles =("){
        //agent.add("Si desea participar debe aceptar los terminos (www.terminos.com/condiciones) para el procesamiento de su informacion. Acepta?")
        agent.add(" y aceptar los términos y condiciones aquí: wwww.terminos.com/condicones ¿Aceptas?");
      }else{
        agent.add("Para recibir una notificación de una promocion vigente debe aceptar los terminos (www.terminos.com/condiciones) para el procesamiento de su informacion, Acepta?")
      }
      
      
      agent.context.set({name:'input_aceptar_condiciones',lifespan:2})
      console.log("=>mostrar promocion--")
      console.log(agent.context.get('start_oferta'))
      console.log("<=")
    }
    
    async function save_dni(agent){
      let re_dni = /[1-9]{1}[0-9]{7}/i
      let re_ext = /[0-9]{9}/i

      let val_dni = agent.query.match(re_dni)
      let val_ext = agent.query.match(re_ext)
      console.log("--line:114--")
      console.log(agent.query)
      console.log("<=")
      if(val_dni | val_ext){
        agent.context.set({name:'input_email',lifespan:2})
        agent.context.delete('input_dni')
        
        let full_name = await load_name(val_dni | val_ext)
        console.log("=== FULL_NAME ===")
        console.log(full_name)
        if(full_name){
          const { nombres, apellidoPaterno, apellidoMaterno } = full_name
          full_name = `${nombres} ${apellidoPaterno} ${apellidoMaterno}`
          agent.add(`¡Genial ${nombres.substring(0,4)}***! Ahora por favor tu correo`)
          agent.context.set({name:'start_oferta',lifespan:2,parameters:{dni:(val_dni | val_ext),full_name:full_name}})
        }else{
          agent.context.delete('input_email')
          agent.add(`¡Perfecto! Ahora tu nombre y tu apellido :D`)
          agent.context.set({name:'input_nombres',lifespan:2})
          agent.context.set({name:'start_oferta',lifespan:2,parameters:{dni:(val_dni | val_ext),full_name:full_name}})
        }
      }else{
        agent.context.set({name:'input_nombres',lifespan:2})
        agent.add("¡Perfecto! Ahora tu nombre y tu apellido :DDD")
        agent.context.delete('input_dni')
        agent.context.set(
          {name:'start_oferta',lifespan:2,parameters:{dni:(val_dni | val_ext)}}
        )
      }
      console.log("=>save_dni--")
      console.log(agent.context.get('start_oferta'))
      console.log("--line:145--")
      console.log(agent.query)
      console.log("<=")
    }

    /**
     * Agregando funciones la "intent"
     */

    let intentMap = new Map()
    intentMap.set('usuario_pide_info_oferta',mostrar_promocion)
    intentMap.set('capturar_dni',save_dni)
    intentMap.set('capturar_nombres', capturar_nombres)
    intentMap.set('capturar_email', capturar_email)
    intentMap.set('capturar_imagenes', capturar_imagenes)
    
    agent.handleRequest(intentMap)
  }



/**
 * Funciones de consulta  
 * */

const consultar_dni = async (dni) => {
  const params = new URLSearchParams();
  params.append('dni', dni);
  let response 
  await axios.post('https://121analytics.com/utils/index.php/API/get_persona_by_dni/', params).then(
    function(result){
      response = result.data
    }
  )
  ;

  if(response.success){
    return response.persona
  }
  return false
}

const load_name = async (dni) => {
const persona = await consultar_dni(dni)
  return persona
}

const consultar_promo = async () => {
  const call = await axios.post(`http://121analytics.com:2110/promociones_disponibles`)
  return call.data
}

const getPromocionDisponible = async () => {
  const promocion = await consultar_promo()
  return promocion
}


const detect_boleta = async(url)=>{
  console.log("============== url = > "+url)
  const params = new URLSearchParams();
  params.append('urlImagen', url);
  let parametros = JSON.stringify({urlImagen:url})
  let response_2;
  console.log(parametros)

  await axios.post(`http://121analytics.com:6500/urlImagenToText`,{urlImagen:url}).then(
    function(result){
      console.log("================>>>>>>>>>>>>>>>>>>")
      console.log(result.data)
      response_2 = result.data
      
    }
  )
  ;
  console.log("============== detect boleta ==============")
  console.log(response_2)
  console.log("==== fin ")
  return response_2
}
const load_ocr = async(url)=>{
  console.log("============================>>>>>>>>>>>>>>>>>>>  load_ocr")
  const data = await detect_boleta(url)
  return data
}




