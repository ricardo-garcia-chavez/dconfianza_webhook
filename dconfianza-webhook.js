const cors = require('cors')
const express = require('express')
const app = require('express')()
const https = require('https')
const fs = require('fs')
const bodyParser = require("body-parser")


const PORT = 2150

app.use(cors())
app.use(express.json())
app.use(
    bodyParser.urlencoded({
        extended: true 
    })
)

app.get('/', function (req, res) {
    res.send({
        success: false,
        message: 'Cannot access @ GET METHOD'
    })
})



  

/** HANDLING WEBHOOK REQUEST */
app.post('/debug', function (req, res) {
});
app.post('', require('./intents/handler'))

//https.createServer(app).listen(PORT,'localhost')

// app.listen(PORT, function () {
//    console.log('--> Webhook app listening on port '+PORT+'\n');
//  });



https.createServer({
        key: fs.readFileSync('../privkey.pem'),
        cert: fs.readFileSync('../fullchain.pem')
}, app).listen(PORT)
